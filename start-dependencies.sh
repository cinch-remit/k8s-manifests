#!/bin/bash

kubectl delete namespace innovationlabs
cd localdev
kubectl apply -f dev-namespace.yaml

kubectl apply -f elk/.
kubectl apply -f innovationlabs/.