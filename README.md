# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repo contains manifest files that can be used to deploy microservices for innovation labs assignment.
* This repo contains manifests files for [elasticsearch](localdev/elk/elastic-search-workload.yaml), [kibana](localdev/elk/kibana-workload.yaml) and [logstash](localdev/elk/logstash-workload.yaml) along with the config files needed to run an ELK stash.
* This repo contains manifest files for [service a](localdev/innovationlabs/service-a-workload.yaml), [service b](localdev/innovationlabs/service-b-workload.yaml), [gateway](localdev/innovationlabs/gateway-workload.yaml) and all the config files needed to run the services.
* You need to have kubernetes running on your system. Need help? use the [install kubernetes](https://kubernetes.io/docs/setup/) resource
* A prerequisite is to make sure the [innovationlabs namespace](localdev/dev-namespace.yaml) exists before applying the [elk files](localdev/elk) or [microservices files](localdev/innovationlabs)

### How do I get set up? ###
There are 3 simple ways to get started 
1. Git clone this repo into the system/node you intend to run these files. You can apply the needed files. You can use the [start script](start-dependencies.sh) to deploy all files in the right order.
```
sh start-dependencies.sh
```
2. Copy the contents of [deploy application script](deploy-application-from-repo.sh) into a local file with `.sh` extension and run the script.(You should have kubernetes setup on your system).
```
cat <<EOF >localfile.sh
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
EOF 

sh localfile.sh
```
3. You can just copy the text below directly into your terminal.
```
{
echo "delete previous namespace"
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "creating namespace"
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "starting Elastic Logstash..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml

echo "starting innovationlabs services..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
}
```

### Services
#### Kibana
Kibana provides visuals for users who wish to aggregate logs from applications. This setup gets handles logs using the ELK stash. There is no authentication
at the moment for this deployment so by navigating to the right host on port `5601` and appending `/kibana` should take you to the right place.
An example on from a kubernetes cluster running on a local computer(minikube or docker-desktop) would be 
```
localhost:5601/kibana
```

* Log on to the right host and port.
* Input `logstash*` when asked to define index pattern
* Select `@timestamp` for Time Filter field name 
* Click the blue `Create index pattern button`.

#### Service A
This application runs on port `8080` for web and port `9090` for GRPC. For more information on about Service A, see 
[service a documentation](https://bitbucket.org/cinch-remit/service-a/src/master/README.md)

#### Service B
This application runs on port `8081` for web and port `9091` for GRPC. For more information on about Service B, see
[service b documentation](https://bitbucket.org/cinch-remit/service-b/src/master/README.md)

#### Gateway
This application runs on port `8082` for web and port `9092` for GRPC. For more information on about Gateway, see
[gateway documentation](https://bitbucket.org/cinch-remit/gateway/src/master/README.md)


## Information About the application in kubernetes
* Namespace: innovationlabs
* See all pods: `kubectl get po -n innovationlabs -o wide`
```
NAME                             READY   STATUS    RESTARTS   AGE   IP           NODE             NOMINATED NODE   READINESS GATES
elasticsearch-7588487f7f-zbzdd   1/1     Running   0          20m   10.1.5.182   docker-desktop   <none>           <none>
gateway-7c5bd8df8b-gb4l2         1/1     Running   0          20m   10.1.5.187   docker-desktop   <none>           <none>
kibana-bc6cd744b-vg5bx           1/1     Running   0          20m   10.1.5.183   docker-desktop   <none>           <none>
logstash-644668bc55-22j7r        1/1     Running   0          20m   10.1.5.184   docker-desktop   <none>           <none>
service-a-79df48c968-ct9c6       1/1     Running   0          20m   10.1.5.185   docker-desktop   <none>           <none>
service-b-77bbf47cc4-ghs8h       1/1     Running   0          20m   10.1.5.186   docker-desktop   <none>           <none>
```
* See all configmaps: `kubectl get configmaps -n innovationlabs -o wide`
```
NAME                 DATA   AGE
gateway-config       4      20m
kube-root-ca.crt     1      21m
logstash-configmap   2      21m
service-a-config     3      20m
service-b-config     3      20m
```
* See all deployments: `kubectl get deployments.apps -n innovationlabs -o wide`
```
NAME            READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS      IMAGES                                      SELECTOR
elasticsearch   1/1     1            1           21m   elasticsearch   cinchremit/elastic-search:latest            app=elasticsearch
gateway         1/1     1            1           21m   gateway         cinchremit/gateway:master                   app=gateway
kibana          1/1     1            1           21m   kibana          docker.elastic.co/kibana/kibana:7.7.0       app=kibana
logstash        1/1     1            1           21m   logstash        docker.elastic.co/logstash/logstash:7.7.0   app=logstash
service-a       1/1     1            1           21m   service-a       cinchremit/service-a:master                 app=service-a
service-b       1/1     1            1           21m   service-b       cinchremit/service-b:master                 app=service-b
```
* See all services: `kubectl get svc -n innovationlabs -o wide`
```
NAME            TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                         AGE   SELECTOR
elasticsearch   LoadBalancer   10.99.185.85     localhost     9200:30384/TCP                  21m   app=elasticsearch
gateway-svc     LoadBalancer   10.110.31.132    localhost     8082:31700/TCP                  21m   app=gateway
kibana          LoadBalancer   10.100.176.129   localhost     5601:30976/TCP                  21m   app=kibana
logstash        LoadBalancer   10.102.33.47     <pending>     5000:30864/TCP                  21m   app=logstash
service-a-svc   LoadBalancer   10.99.149.180    localhost     8080:31590/TCP,9090:31611/TCP   21m   app=service-a
service-b-svc   LoadBalancer   10.111.15.150    localhost     8081:31881/TCP,9091:31503/TCP   21m   app=service-b
```


## Caveats
Definitely not a production ready kubernetes setup. Will setup role,rolebinding & serviceaccount to limit access within the cluster. Eventually some networkpolicies. 
### Who do I talk to? ⬇️

* Robinson Mgbah ==== mgbahacho@aol.com