#!/bin/bash

echo "delete previous namespace"
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "creating namespace"
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "starting Elastic Logstash..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml

echo "starting innovationlabs services..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
